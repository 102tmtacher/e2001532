public class Ticket{   

public static void main(String[] args) {   
	System.out.println("Data from IoT Ticket");
	System.out.println("Moikkaukset tomilta");
	Register();
	ReadMeas();
}


public static void Register(){
		  final String userName = "student2020";
		    final String pwd = "student2020_Password";
		    final IOTAPIClient client = new IOTAPIClient("https://my.iot-ticket.com/api/v1", userName, pwd);

		    Ticket d = new Ticket();
		    d.setName("TominDevice");
		    d.setManufacturer("Tomi OY");
		    d.setType("DeviceType");
		    d.getAttributes().add(new DeviceAttribute("DeviceHeight", "15.0"));

		    DeviceDetails deviceDetails = client.registerDevice(d);
		    System.out.println("This is the device id that should be used in subsequent calls when sending measurement data: " + deviceDetails.getDeviceId());
		}
	
public static void ReadMeas() {
		 DatanodeQueryCriteria crit = new DatanodeQueryCriteria(deviceDetails.getDeviceId(), "Temperature", "TemperatureTurku ", "TemperatureVaasa");
		    crit.setLimit(50);
		    crit.setSortOrder(Order.Descending);


		    ProcessValues processValues = client.readProcessData(crit);
		    Collection<DatanodeRead> datanodeReads = processValues.getDatanodeReads();
		    for (DatanodeRead datanodeRead : datanodeReads) {

		        for (DatanodeReadValue value : datanodeRead.getDatanodeReadValues()) {
		            System.out.println("Value=" + value.getValue() + " @" +value.getTimestampMilliSeconds());

		        }
		    }
	}